oc delete project todoapp-reviewservice
RETRIES=5
until oc new-project todoapp-reviewservice
 do
   let RETRIES=RETRIES-1
   echo "try again $RETRIES"
  sleep 1
  if [ "$RETRIES" = "0" ];then
    exit 1;
  fi
done
oc new-app --name tododb --docker-image registry.access.redhat.com/rhscl/mysql-57-rhel7 \
 --insecure-registry -e MYSQL_USER=todoapp -e MYSQL_PASSWORD=mypass -e MYSQL_DATABASE=todo
oc set triggers dc/tododb --from-config --remove
oc create secret generic tododb --from-literal=uesr=todoapp --from-literal=password=mypass
oc set env dc/tododb --prefix MYSQL_ --from secret/tododb
oc set env dc/tododb --list
#Change redeployment strategy and add the post-hook
oc patch dc/tododb --patch '{"spec":{"strategy":{"type":"Recreate"}}}'
oc patch dc/tododb --type=json -p='[{"op":"remove", "path":"/spec/strategy/rollingParams"}]'
oc patch dc/tododb --patch \
'{"spec":{"strategy":{"recreateParams":{"post":{"failurePolicy": "Abort","execNewPod":{"containerName":"tododb","command":["/bin/sh","-c","curl -s https://bitbucket.org/rhuniversity/todoapp-openshift-guide/raw/master/import.sh -o /tmp/import.sh&&chmod 755 /tmp/import.sh&&/tmp/import.sh"]}}}}}}'
oc rollout latest dc/tododb

#Deploy todo images
#skopeo inspect --tls-verify=false docker://registry.access.redhat.com/rhscl/nodejs-6-rhel7|grep s2i
oc new-app --name backend -e DATABASE_NAME=todo -e DATABASE_USER=todoapp \
  -e DATABASE_PASSWORD=mypass -e DATABASE_SVC=tododb \
  https://bitbucket.org/rhuniversity/todo-backend.git

oc set triggers --from-config --remove dc/backend
oc set env dc/backend --prefix=DATABASE_ --from=secret/tododb
oc create cm todoapp --from-literal init=false
oc set env dc/backend --prefix=DATABASE_ --from=cm/todoapp
oc rollout latest dc/backend
oc rsh backend-4-khgqv env |grep DATABASE_
oc expose svc backend --hostname backend.ryanlab.redhat.com

#oc port-forward tododb-2-nf5bm 30306:3306
#mysql -utodoapp -pmypass -P30306 -h127.0.0.1<todo.sql
#Build from Dockerfile, note that the rest api url in the item.js
oc new-app --name=frontend  https://bitbucket.org/rhuniversity/todo-frontend.git
oc expose svc backend --hostname frontend.ryanlab.redhat.com

curl http://backend.ryanlab.redhat.com/todo/api/items-count
