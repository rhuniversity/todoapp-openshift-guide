#!/usr/bin/env bash
#oc project test-php
oc delete project todoapp-reviewtemplate
RETRIES=5
until oc new-project todoapp-reviewtemplate
do
  let RETRIES=RETRIES-1
  echo "try again $RETRIES"
  sleep 1
  if [ "$RETRIES" = "0" ];then
    exit 1;
  fi
done

oc create -f todoapp.yaml
oc new-app todoapp -e PASSWORD=redhat -e CLEAN_DATABASE=false
